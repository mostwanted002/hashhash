from flask import Flask, jsonify, request, render_template
from Crypto.Hash import SHA224, SHA256, SHA384, SHA512, SHA3_224, SHA3_256, SHA3_384, SHA3_512, SHA1, MD2, MD5
import binascii
app = Flask(__name__)
algos = {'sha224': SHA224.new(), 'sha256': SHA256.new(), 'sha384':SHA384.new(), 'sha512':SHA512.new(), 'sha3-224':SHA3_224.new(), 'sha3-256':SHA3_256.new(), 'sha3-384':SHA3_384.new(), 'sha3-512':SHA3_512.new(), 'sha1':SHA1.new(), 'md2':MD2.new(), 'md5':MD5.new()}
@app.route('/', methods=['GET', 'POST'])
def hashhash():
	data = ''
	if(request.method == 'GET'):
		data = {"usage":{"method":"POST", "Content-Type":"application/json" ,"data":"{\"algo\":\"<algorithm>\", \"text\":\"<text>\"}"}, "algorithms":"sha224, sha256, sha384, sha512, sha3-244, sha3-256, sha3-384, sha3-512, sha1, md2, md5"}
	else:
		try:
			received = request.json
			algo = received['algo']
			text = received['text'].encode()
			hasher = algos[algo]
			hasher.update(text)
			data = {"text":text.decode(),"algorithm":algo ,"digest":binascii.hexlify(hasher.digest()).decode()}
		except:
			data = {"Error":"Invalid algorithm, format, or Content-Type"}
	return jsonify(data)

