FROM python:3.6
WORKDIR /usr/app/
COPY app .
RUN pip3 install -r requirements.txt
EXPOSE 4000
CMD gunicorn -w 4 -b 0.0.0.0:4000 app:app